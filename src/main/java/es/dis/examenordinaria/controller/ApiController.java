package es.dis.examenordinaria.controller;

import com.google.gson.Gson;
import es.dis.examenordinaria.dto.*;
import es.dis.examenordinaria.service.ApiService;
import es.dis.examenordinaria.utils.Methods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class ApiController {


    private Map<String, Integer> registro = new HashMap<>();

    @Autowired
    ApiService apiService;


    @PostMapping(name = "/buscar", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> buscarIp(@RequestBody BuscarIpDto body){

        LocalizaIpDto localizaIpDto = apiService.leerJson();

        for(DatosEspecificoDto datosEspecificoDto : localizaIpDto.getDatos()){

            Boolean perteneceAlRango = Methods.perteneceAlRango(body.getIp(),
                    Methods.longToIp(datosEspecificoDto.getIp_to()),
                    Methods.longToIp(datosEspecificoDto.getIp_from()));

            if(!registro.containsKey(body.getIp())){
                registro.put(body.getIp(), 1);
            }else{
                Integer cont = registro.get(body.getIp());
                registro.put(body.getIp(), cont ++);
            }

            if(perteneceAlRango){

                BusquedaResponseDto busquedaResponseDto = new BusquedaResponseDto(
                        body.getIp(),
                        datosEspecificoDto.getCountry_code(),
                        datosEspecificoDto.getRegion_name(),
                        datosEspecificoDto.getCity_name(),
                        datosEspecificoDto.getZip_code(),
                        datosEspecificoDto.getTime_zone()
                );

                String json = new Gson().toJson(busquedaResponseDto);

                return ResponseEntity.status(HttpStatus.OK).body(json);

            }else{
                BusquedaResponseErrorDto busquedaResponseErrorDto = new BusquedaResponseErrorDto(body.getIp(), "Ip no encontrada");
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Gson().toJson(busquedaResponseErrorDto));
            }
        }

        return null;

    }


    @GetMapping(name = "/buscar", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> obtenerBusqueda(){
        return null;
    }


}
