package es.dis.examenordinaria.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LocalizaIpDto {

    private List<DatosEspecificoDto> Datos;

}
