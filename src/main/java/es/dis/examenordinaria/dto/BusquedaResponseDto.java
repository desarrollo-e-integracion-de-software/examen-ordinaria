package es.dis.examenordinaria.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BusquedaResponseDto {

    private String ip;

    private String country_code;

    private String region_name;

    private String city_name;

    private String zip_code;

    private String time_zone;

}
