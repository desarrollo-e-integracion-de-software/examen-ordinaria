package es.dis.examenordinaria.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZoneId;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DatosEspecificoDto {

    private Long ip_from;

    private Long ip_to;

    private String country_code;

    private String country_name;

    private String region_name;

    private String city_name;

    private Double latitude;

    private Double longitude;

    private String zip_code;

    private String time_zone;

}
