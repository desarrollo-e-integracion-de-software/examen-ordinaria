package es.dis.examenordinaria.utils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Methods {

    public static String longToIp(long ip) {
        StringBuilder result = new StringBuilder(15);
        for (int i = 0; i < 4; i++) {
            result.insert(0, Long.toString(ip & 0xff));
            if (i < 3) {
                result.insert(0, '.');
            }
            ip = ip >> 8;
        }
        return result.toString();
        //192.168.1.2 -> 3232235778
    }

    public static Long Dot2LongIP(String dottedIP) {
        String[] addrArray = dottedIP.split("\\.");
        long num = 0;
        for (int i=0;i<addrArray.length;i++) {
            int power = 3-i;
            num += ((Integer.parseInt(addrArray[i]) % 256) * Math.pow(256,power));
        }
        return num;
    }


    /**
     * Dada una ip y dos rangos, devuelve un boolean indicando si pertene o no a ese rango
     * @param ip
     * @param ipFrom
     * @param ipTo
     * @return
     */
    public static Boolean perteneceAlRango(String ip, String ipFrom, String ipTo){
        //ipFrom 192.168.0.1
        //ipTo 192.168.0.123
        //ip que pertence --> 192.168.0.43
        //ip que no pertenece --> 192.168.0.220
        //ip que no pertenece --> 193.XXX.XXX.XXX
        //ip que no pertenece --> 192.167.XXX.XXX
        //ip que no pertenece --> 192.168.1.XXX

        String[] ipFromSplit = ipFrom.split("\\.");
        String[] ipToSplit = ipTo.split("\\.");
        String[] ipSplit = ip.split("\\.");

        if(!validIp(ipSplit))
            return false;


        if(Integer.parseInt(ipSplit[0]) == Integer.parseInt(ipToSplit[0])
            && Integer.parseInt(ipSplit[0]) == Integer.parseInt(ipFromSplit[0])){
            if(Integer.parseInt(ipSplit[1]) == Integer.parseInt(ipToSplit[1])
                    && Integer.parseInt(ipSplit[1]) == Integer.parseInt(ipFromSplit[1])){
                if(Integer.parseInt(ipSplit[2]) == Integer.parseInt(ipToSplit[2])
                        && Integer.parseInt(ipSplit[2]) == Integer.parseInt(ipFromSplit[2])){
                    if(Integer.parseInt(ipSplit[3]) <= Integer.parseInt(ipToSplit[3])
                            && Integer.parseInt(ipSplit[3]) >= Integer.parseInt(ipFromSplit[3])){
                        return true;
                    }
                }
            }
        }//End primer if
        return false;
    }

    /**
     * Comprueba que los valores de la IP van de 0 a 255
     * @param ip
     * @return
     */
    private static boolean validIp(String[] ip){
        //Esto es para tener todo convertido a lista de integers en lugar de strings
        List<Integer> ipConverted = Arrays.stream(ip).map(k -> Integer.parseInt(k)).collect(Collectors.toList());
        return ipConverted.stream().allMatch(k -> k >= 0 && k < 256);

    }


}