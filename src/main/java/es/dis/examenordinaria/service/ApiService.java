package es.dis.examenordinaria.service;

import com.google.gson.Gson;
import es.dis.examenordinaria.dto.LocalizaIpDto;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class ApiService {

    public ApiService() {
    }

    public LocalizaIpDto leerJson(){
        try {
            String content = Files.readString(Paths.get("src/main/resources/LocalizaIP.json"), StandardCharsets.UTF_8);
            return new Gson().fromJson(content, LocalizaIpDto.class);
        } catch (IOException e) {
            System.err.println("Error al leer el fichero Json --> " + e.getMessage());
            return null;
        }
    }
}
