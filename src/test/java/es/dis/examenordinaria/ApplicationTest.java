package es.dis.examenordinaria;


import es.dis.examenordinaria.utils.Methods;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;

@SpringBootTest
public class ApplicationTest {

    ArrayList<String> listaDeIps;

    @Before
    public void before(){
        //Dentro de este método haria todas las inicializaciones
        //Imaginad que tuviese que hacer los test con algunas listas pues las listas y sus contenidos
        //para hacer las pruebas los inicializaria aqui.

        listaDeIps = new ArrayList<>();
        listaDeIps.add("1.2.3.4");
        //Y asi sucesivamente, y asi usaria ese array de listaDeIps para probar.
    }


    @Test
    public void test(){
        String ip = "0.0.0.0";
        Assert.assertFalse(Methods.perteneceAlRango(ip, "1.1.1.1", "1.1.1.2"));
    }

    @Test
    public void test2(){
        String ip = "0.0.0.150";
        Assert.assertTrue(Methods.perteneceAlRango(ip, "0.0.0.1", "0.0.0.240"));
    }

    @Test
    public void test3(){
        String a = "abcd";
        String b = "bcda";
        Assert.assertNotEquals(a,b);
        Assert.assertEquals(a,a);
    }

    //Le pongo esto para decir que el test se cumple si salta esta excepcion
    @Test(expected = IndexOutOfBoundsException.class)
    public void test4(){
        Integer[] list = new Integer[1];

        //Suelta una excepcion xq la lista es de un solo elemento e intento acceder al cuarto
        Integer a = list[4];

    }

}
